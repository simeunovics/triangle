import { State as LinksState } from '../redux/links/index';
import {
  State as PreferencesState,
  AvailableThemes,
} from '../redux/preferences/index';
import { State as ViewModeState } from '../redux/viewMode/index';
import { State as HomePageFilters } from '../redux/homePageLinkFilters/index';

interface CurrentStateShape {
  links: LinksState;
  preferences: PreferencesState;
  viewMode: ViewModeState;
  homePageLinkFilters: HomePageFilters;
}

interface LinkMeta {
  icon: String | null;
  provider: String | null;
}
interface LinkMeta {
  icon: String | null;
  provider: String | null;
}
interface Tag {
  id: String;
  text: String;
  color: String;
}
interface Link {
  id: String;
  name: String;
  url: String;
  clicks?: String[];
  tags?: Tag[];
  metadata?: LinkMeta;
}
interface TagFilter {
  text: String;
  color: String;
}
interface StoredStateShape {
  links: { custom: Link[] };
  preferences: {
    sortLinksByUsage: Boolean;
    openLinksInNewTab: Boolean;
    theme?: AvailableThemes;
  };
  viewMode: {
    controlMode: Boolean;
    previewMode: Boolean;
  };
  homePageLinkFilters: {
    searchQuery: String;
    searchFieldFocused: Boolean;
    includedTags: TagFilter[];
  };
}

export const applyTransitions = (
  state: StoredStateShape,
): CurrentStateShape => {
  const transformed: CurrentStateShape = {
    ...state,
    preferences: {
      ...state.preferences,
      theme:
        state.preferences.theme === undefined
          ? AvailableThemes.Light
          : state.preferences.theme,
    },
    links: {
      ...state.links,
      custom: state.links.custom.map(link => {
        const metadata =
          link.metadata !== undefined
            ? link.metadata
            : { icon: null, provider: null };
        const clicks = link.clicks || [];
        const tags = link.tags || [];
        return {
          ...link,
          metadata,
          clicks,
          tags,
        };
      }),
    },
  };

  return transformed;
};
