import { applyTransitions } from './stateLoader';
export * from './stateLoader';
export const fuzzySearch = (needle = '', obj = {}) => {
  const matched = [];
  for (const key in obj) {
    if (!obj.hasOwnProperty(key) || !obj[key]) continue;
    if (
      obj[key]
        .toString()
        .toLowerCase()
        .indexOf(needle.toLowerCase()) !== -1
    ) {
      matched.push(obj);
    }
  }
  return matched.length;
};

export const isStringUrl = value => {
  try {
    const url = new URL(value);
    return Boolean(url);
  } catch (e) {
    return false;
  }
};

export const isInputFieldInFocus = () => {
  const { activeElement } = window.document;
  if (!activeElement) {
    return false;
  }

  const { type } = activeElement;
  if (!type) {
    return false;
  }

  return type.includes('text');
};

export const destroyLocalCache = () => window.localStorage.clear();

export const loadState = () => {
  try {
    return (
      applyTransitions(JSON.parse(localStorage.getItem('state'))) || undefined
    );
  } catch (err) {
    console.error(err);
    return undefined;
  }
};

export const persistState = state => {
  try {
    localStorage.setItem('state', JSON.stringify(state));
  } catch (err) {
    window.console.log(err);
  }
};
