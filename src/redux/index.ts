import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { loadState, persistState } from '../helpers/index';

import links from './links/index';
import viewMode from './viewMode/index';
import preferences from './preferences/index';
import homePageLinkFilters from './homePageLinkFilters/index';

const reducers = combineReducers({
  links,
  preferences,
  viewMode,
  homePageLinkFilters,
});

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: Function;
  }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line no-underscore-dangle,max-len
const store = createStore(
  reducers,
  loadState(),
  composeEnhancers(applyMiddleware(thunk)),
);

store.subscribe(() => persistState(store.getState()));

export default store;
