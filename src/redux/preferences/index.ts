export enum AvailableThemes {
  Light = 'Light',
  Dark = 'Dark',
}

// State
export interface State {
  sortLinksByUsage: Boolean;
  openLinksInNewTab: Boolean;
  theme: AvailableThemes;
}
enum AvailableActions {
  SET_SORTING_LINKS_BY_USAGE_FLAG = 'triangle/preferences/SET_SORTING_LINKS_BY_USAGE_FLAG',
  SET_OPEN_LINKS_IN_NEW_TAB_FLAG = 'triangle/preferences/SET_OPEN_LINKS_IN_NEW_TAB_FLAG',
  SET_THEME = 'triangle/preferences/SET_THEME',
}
interface SetSortingLinksByUsageFlag {
  type: AvailableActions.SET_SORTING_LINKS_BY_USAGE_FLAG;
  flag: Boolean;
}
interface SetOpenLinksInNewTabFlag {
  type: AvailableActions.SET_OPEN_LINKS_IN_NEW_TAB_FLAG;
  flag: Boolean;
}
interface SetTheme {
  type: AvailableActions.SET_THEME;
  theme: AvailableThemes;
}

// Reducers
const initState: State = {
  sortLinksByUsage: false,
  openLinksInNewTab: false,
  theme: AvailableThemes.Light,
};
export default function(
  state: State = initState,
  action: SetSortingLinksByUsageFlag | SetOpenLinksInNewTabFlag | SetTheme,
): State {
  switch (action.type) {
    case AvailableActions.SET_SORTING_LINKS_BY_USAGE_FLAG:
      return {
        ...state,
        sortLinksByUsage: action.flag,
      };
    case AvailableActions.SET_OPEN_LINKS_IN_NEW_TAB_FLAG:
      return {
        ...state,
        openLinksInNewTab: action.flag,
      };
    case AvailableActions.SET_THEME:
      return {
        ...state,
        theme: action.theme,
      };
    default:
      return state;
  }
}

export const setSortingLinksByUsageFlag = (
  flag: Boolean,
): SetSortingLinksByUsageFlag => ({
  type: AvailableActions.SET_SORTING_LINKS_BY_USAGE_FLAG,
  flag,
});
export const setOpenLinksInNewTabFlag = (
  flag: Boolean,
): SetOpenLinksInNewTabFlag => ({
  type: AvailableActions.SET_OPEN_LINKS_IN_NEW_TAB_FLAG,
  flag,
});
export const setTheme = (theme: AvailableThemes): SetTheme => ({
  type: AvailableActions.SET_THEME,
  theme,
});
