export interface State {
  controlMode: Boolean;
  previewMode: Boolean;
}
// Actions
enum AvailableActions {
  TOGGLE_CONTROL_MODE = 'triangle/viewMode/TOGGLE_CONTROL_MODE',
  ENABLE_PREVIEW_MODE = 'triangle/viewMode/ENABLE_PREVIEW_MODE',
  DISABLE_PREVIEW_MODE = 'triangle/viewMode/DISABLE_PREVIEW_MODE',
}
interface ToggleControlMode {
  type: AvailableActions.TOGGLE_CONTROL_MODE;
}
interface EnablePreviewMode {
  type: AvailableActions.ENABLE_PREVIEW_MODE;
}
interface DisablePreviewMode {
  type: AvailableActions.DISABLE_PREVIEW_MODE;
}

// Reducers
const initialState: State = {
  controlMode: false,
  previewMode: false,
};
export default function(
  state: State = initialState,
  action: ToggleControlMode | EnablePreviewMode | DisablePreviewMode,
): State {
  switch (action.type) {
    case AvailableActions.TOGGLE_CONTROL_MODE:
      return {
        ...state,
        controlMode: !state.controlMode,
      };
    case AvailableActions.ENABLE_PREVIEW_MODE:
      return {
        ...state,
        previewMode: true,
      };
    case AvailableActions.DISABLE_PREVIEW_MODE:
      return {
        ...state,
        previewMode: false,
      };
    default:
      return state;
  }
}

// Action Creators
export const toggleControlMode = (): ToggleControlMode => ({
  type: AvailableActions.TOGGLE_CONTROL_MODE,
});
export const enablePreviewMode = (): EnablePreviewMode => ({
  type: AvailableActions.ENABLE_PREVIEW_MODE,
});
export const disablePreviewMode = (): DisablePreviewMode => ({
  type: AvailableActions.DISABLE_PREVIEW_MODE,
});
