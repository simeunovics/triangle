import uuidv4 from 'uuid/v4';

// State
export interface State {
  custom: Link[];
}
interface Link {
  id: String;
  name: String;
  url: String;
  clicks: LinkClick[];
  tags: Tag[];
  metadata: LinkMeta;
}
interface LinkMeta {
  icon: String | null;
  provider: String | null;
}
interface LinkClick {}
interface Tag {
  id: String;
  text: String;
  color: String;
}

// Actions
enum AvailableActions {
  ADD = 'triangle/links/ADD',
  REMOVE = 'triangle/links/REMOVE',
  EDIT_NAME = 'triangle/links/EDIT_NAME',
  EDIT_URL = 'triangle/links/EDIT_URL',
  RECORD_LINK_CLICK = 'triangle/links/RECORD_LINK_CLICK',
  SET_LINK_META = 'triangle/links/SET_LINK_META',
  ADD_TAG = 'triangle/links/ADD_TAG',
  REMOVE_TAG = 'triangle/links/REMOVE_TAG',
}
interface Add {
  type: AvailableActions.ADD;
  link: Link;
}
interface Remove {
  type: AvailableActions.REMOVE;
  id: String;
}
interface EditName {
  type: AvailableActions.EDIT_NAME;
  id: String;
  name: String;
}
interface EditLinkUrl {
  type: AvailableActions.EDIT_URL;
  id: String;
  url: String;
}
interface RecordLinkClick {
  type: AvailableActions.RECORD_LINK_CLICK;
  id: String;
  date: Date;
}
interface SetLinkMeta {
  type: AvailableActions.SET_LINK_META;
  id: String;
  meta: LinkMeta;
}
interface AddTag {
  type: AvailableActions.ADD_TAG;
  linkId: String;
  tag: Tag;
}
interface RemoveTag {
  type: AvailableActions.REMOVE_TAG;
  linkId: String;
  tagId: String;
}

// Reducer
const initialState: State = {
  custom: [],
};
export default function reducer(
  state = initialState,
  action:
    | Add
    | Remove
    | EditName
    | EditLinkUrl
    | RecordLinkClick
    | SetLinkMeta
    | AddTag
    | RemoveTag,
): State {
  switch (action.type) {
    case AvailableActions.ADD:
      return {
        ...state,
        custom: [...state.custom, action.link],
      };
    case AvailableActions.REMOVE:
      return {
        ...state,
        custom: state.custom.filter(link => link.id !== action.id),
      };
    case AvailableActions.EDIT_NAME:
      return {
        ...state,
        custom: state.custom.map(link => {
          if (link.id !== action.id) {
            return link;
          }

          return { ...link, name: action.name };
        }),
      };
    case AvailableActions.EDIT_URL:
      return {
        ...state,
        custom: state.custom.map(link => {
          if (link.id !== action.id) {
            return link;
          }

          return { ...link, url: action.url };
        }),
      };
    case AvailableActions.RECORD_LINK_CLICK:
      return {
        ...state,
        custom: state.custom.map(link => {
          if (link.id !== action.id) {
            return link;
          }

          return {
            ...link,
            clicks: [action.date, ...link.clicks],
          };
        }),
      };
    case AvailableActions.SET_LINK_META:
      return {
        ...state,
        custom: state.custom.map(link => {
          if (link.id !== action.id) {
            return link;
          }

          return {
            ...link,
            metadata: action.meta,
          };
        }),
      };
    case AvailableActions.ADD_TAG:
      return {
        ...state,
        custom: state.custom.map(link => {
          if (link.id !== action.linkId) {
            return link;
          }

          return {
            ...link,
            tags: [action.tag, ...link.tags],
          };
        }),
      };
    case AvailableActions.REMOVE_TAG:
      return {
        ...state,
        custom: state.custom.map(link => {
          if (link.id !== action.linkId) {
            return link;
          }

          return {
            ...link,
            tags: link.tags.filter(tag => tag.id !== action.tagId),
          };
        }),
      };
    default:
      return state;
  }
}

// Action Creators
interface DispatchFunction<T, K> {
  (arg: T): K;
}
export const addLink = (name: string, url: string): Function => (
  dispatch: DispatchFunction<Add, State>,
): State =>
  dispatch({
    type: AvailableActions.ADD,
    link: {
      id: uuidv4(),
      name,
      url,
      clicks: [],
      tags: [],
      metadata: {
        icon: null,
        provider: null,
      },
    },
  });
export const removeLink = (id: String): Remove => ({
  type: AvailableActions.REMOVE,
  id,
});
export const editLinkName = (id: String, name: String): EditName => ({
  type: AvailableActions.EDIT_NAME,
  id,
  name,
});
export const editLinkUrl = (id: String, url: String): EditLinkUrl => ({
  type: AvailableActions.EDIT_URL,
  id,
  url,
});
export const recordLinkClick = (id: String): RecordLinkClick => ({
  type: AvailableActions.RECORD_LINK_CLICK,
  id,
  date: new Date(),
});
export const setLinkMeta = (id: String, meta: LinkMeta): SetLinkMeta => ({
  type: AvailableActions.SET_LINK_META,
  id,
  meta,
});
export const addTag = (linkId: String, tag: Tag): AddTag => ({
  type: AvailableActions.ADD_TAG,
  linkId,
  tag,
});
export const removeTag = (linkId: String, tagId: String): RemoveTag => ({
  type: AvailableActions.REMOVE_TAG,
  linkId,
  tagId,
});
