// Interfaces
export interface State {
  searchQuery: String;
  searchFieldFocused: Boolean;
  includedTags: TagFilter[];
}
interface TagFilter {
  text: String;
  color: String;
}
enum AvailableActions {
  SET_FOCUS_ON_SEARCH_FIELD = 'triangle/homePageLinkFilters/SET_FOCUS_ON_SEARCH_FIELD',
  UPDATE_SEARCH_QUERY = 'triangle/homePageLinkFilters/UPDATE_SEARCH_QUERY',
  ADD_TAG_TO_FILTER = 'triangle/homePageLinkFilters/ADD_TAG_TO_FILTER',
  REMOVE_TAG_FROM_FILTER = 'triangle/homePageLinkFilters/REMOVE_TAG_FROM_FILTER',
}
interface SetFocusOnSearchField {
  type: AvailableActions.SET_FOCUS_ON_SEARCH_FIELD;
  payload: {
    focus: Boolean;
  };
}
interface UpdateSearchQuery {
  type: AvailableActions.UPDATE_SEARCH_QUERY;
  payload: {
    query: String;
  };
}
interface AddTagToFilter {
  type: AvailableActions.ADD_TAG_TO_FILTER;
  tag: TagFilter;
}
interface RemoveTagFromFilter {
  type: AvailableActions.REMOVE_TAG_FROM_FILTER;
  tag: TagFilter;
}

const initialState: State = {
  searchQuery: '',
  searchFieldFocused: false,
  includedTags: [],
};

// reducers
export default function reducer(
  state: State = initialState,
  action:
    | SetFocusOnSearchField
    | UpdateSearchQuery
    | AddTagToFilter
    | RemoveTagFromFilter,
): State {
  switch (action.type) {
    case AvailableActions.SET_FOCUS_ON_SEARCH_FIELD:
      return {
        ...state,
        searchFieldFocused: action.payload.focus,
      };
    case AvailableActions.UPDATE_SEARCH_QUERY:
      return {
        ...state,
        searchQuery: action.payload.query,
      };
    case AvailableActions.ADD_TAG_TO_FILTER:
      const currentTags = state.includedTags || [];
      if (currentTags.map(tag => tag.text).includes(action.tag.text)) {
        return { ...state };
      }
      return {
        ...state,
        includedTags: [action.tag, ...currentTags],
      };
    case AvailableActions.REMOVE_TAG_FROM_FILTER:
      return {
        ...state,
        includedTags: state.includedTags.filter(
          tag => tag.text !== action.tag.text,
        ),
      };
    default:
      return { ...state };
  }
}

// action creators
export const setFocusOnSearchField = (
  focus: Boolean,
): SetFocusOnSearchField => ({
  type: AvailableActions.SET_FOCUS_ON_SEARCH_FIELD,
  payload: { focus },
});
export const updateSearchQuery = (query: String): UpdateSearchQuery => ({
  type: AvailableActions.UPDATE_SEARCH_QUERY,
  payload: { query },
});
export const addTagToFilter = (tag: TagFilter): AddTagToFilter => ({
  type: AvailableActions.ADD_TAG_TO_FILTER,
  tag,
});
export const removeTagToFilter = (tag: TagFilter): RemoveTagFromFilter => ({
  type: AvailableActions.REMOVE_TAG_FROM_FILTER,
  tag,
});
