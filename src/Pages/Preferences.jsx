import React from 'react';
import { connect } from 'react-redux';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import { FiCheckCircle, FiCircle } from 'react-icons/fi';
import { PageWithTopNav } from '../Components/Layouts';
import TopNavigation from '../Components/TopNavigation';
import { TextButton } from '../Components/Buttons';
import {
  setSortingLinksByUsageFlag,
  setOpenLinksInNewTabFlag,
  setTheme,
} from '../redux/preferences/index';

const Page = styled.section`
  padding: 20px;
`;
const Headline = styled.h1`
  color: ${props => props.theme.colors._6.hex};
`;
const Option = styled.div`
  display: grid;
  grid-template-columns: 50px 1fr;
  grid-gap: 5px;
`;
const Label = styled.p`
  display: flex;
  align-items: center;
  color: ${props => props.theme.colors._6.hex};
`;
const Switch = styled(TextButton)`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  font-size: ${props => props.theme.fontSize.normal};
  color: ${props => props.theme.colors._6.hex};
`;

const Preferences = props => (
  <PageWithTopNav>
    <TopNavigation />
    <Page>
      <Headline>Preferences</Headline>
      <section>
        <Option>
          <Switch
            onClick={() =>
              props.setSortingLinksByUsageFlag(!props.sortLinksByUsage)
            }
          >
            {props.sortLinksByUsage ? <FiCheckCircle /> : <FiCircle />}
          </Switch>
          <Label>Sort Links By Their Usage</Label>
        </Option>
        <Option>
          <Switch
            onClick={() =>
              props.setOpenLinksInNewTabFlag(!props.openLinksInNewTab)
            }
          >
            {props.openLinksInNewTab ? <FiCheckCircle /> : <FiCircle />}
          </Switch>
          <Label>Open Links In New Tab</Label>
        </Option>
        <Option>
          <Switch
            onClick={() =>
              props.setTheme(props.theme === 'Dark' ? 'Light' : 'Dark')
            }
          >
            {props.theme === 'Dark' ? <FiCheckCircle /> : <FiCircle />}
          </Switch>
          <Label>Use Dark theme (page reload required)</Label>
        </Option>
      </section>
    </Page>
  </PageWithTopNav>
);
Preferences.propTypes = {
  sortLinksByUsage: PropTypes.bool.isRequired,
  openLinksInNewTab: PropTypes.bool.isRequired,
  setSortingLinksByUsageFlag: PropTypes.func.isRequired,
  setOpenLinksInNewTabFlag: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  sortLinksByUsage: state.preferences.sortLinksByUsage,
  openLinksInNewTab: state.preferences.openLinksInNewTab,
  theme: state.preferences.theme,
});

const mapDispatchToProps = dispatch => ({
  setSortingLinksByUsageFlag: flag =>
    dispatch(setSortingLinksByUsageFlag(flag)),
  setOpenLinksInNewTabFlag: flag => dispatch(setOpenLinksInNewTabFlag(flag)),
  setTheme: theme => dispatch(setTheme(theme)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Preferences);
