import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from '@emotion/styled/macro';
import PropTypes from 'prop-types';
import { IoMdSearch, IoMdClose } from 'react-icons/io';
import { FiFilter } from 'react-icons/fi';
import {
  setFocusOnSearchField,
  updateSearchQuery,
  removeTagToFilter,
} from '../../redux/homePageLinkFilters/index';
import { isInputFieldInFocus } from '../../helpers/index';

const SEARCH_ICON_SIZE = 50;

const Grid = styled.div`
  display: grid;
  grid-template-columns: ${SEARCH_ICON_SIZE}px ${SEARCH_ICON_SIZE / 3}px 1fr auto;
  width: 100%;
`;
const Container = styled.div`
  margin: 30px 0;
  display: flex;
  align-items: center;
  background-color: ${props => props.theme.colors._2.hex};
  border-radius: 40px;
`;
const Icon = styled.div`
  cursor: pointer;
  color: ${props => props.theme.colors._4.hex};
  width: ${SEARCH_ICON_SIZE}px;
  height: ${SEARCH_ICON_SIZE}px;
  padding-left: 4px;
  font-size: ${props => props.theme.fontSize.large};
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Input = styled.input`
  background-color: transparent;
  transition: all ${props => props.theme.animationDuration.short};
  border: none;
  font-size: ${props => props.theme.fontSize.small};
  outline: 0;
  margin-top: -3px;
  color: ${props => props.theme.colors._4.hex};
  opacity: ${props => props.theme.opacity.high};
  :focus {
    opacity: 1;
  }
`;
const VerticalSeparator = styled.div`
  width: 2px;
  background-color: ${props => props.theme.colors._5.rgba(0.1)};
  margin-top: ${SEARCH_ICON_SIZE / 4}px;
  height: ${SEARCH_ICON_SIZE / 2}px;
`;
const TagList = styled.div`
  display: flex;
  flex-direction: row-reverse;
  align-items: center;
  justify-content: flex-start;
  svg {
    margin-right: 15px;
    margin-top: 3px;
    opacity: ${props => props.theme.opacity.low};
    color: ${props => props.theme.colors._6.hex};
  }
`;
const Tag = styled.button`
  background: transparent;
  border: none;
  font-size: ${props => props.theme.fontSize.normal};
  color: ${props => props.theme.colors._4.hex};
  cursor: pointer;
`;

class Search extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    searchQuery: PropTypes.string.isRequired,
    setFocus: PropTypes.func.isRequired,
    focused: PropTypes.bool.isRequired,
  };

  constructor() {
    super();
    this.textInput = React.createRef();
  }

  componentDidMount() {
    window.addEventListener('keyup', this.focusOnSpecificKey);
    window.addEventListener('keyup', this.blurOnSpecificKey);
  }

  componentWillUnmount() {
    window.removeEventListener('keyup', this.focusOnSpecificKey);
    window.removeEventListener('keyup', this.blurOnSpecificKey);
  }

  blurOnSpecificKey = e => {
    if (![27].includes(e.keyCode)) {
      return;
    }

    this.blur();
  };

  focusOnSpecificKey = e => {
    if (isInputFieldInFocus()) {
      return;
    }

    if (this.props.focused) {
      return;
    }
    if (![83, 70, 191].includes(e.keyCode)) {
      return;
    }

    this.focusInput();
  };

  blur = () => {
    this.textInput.current.blur();
    this.props.setFocus(false);
  };

  focusInput = () => {
    this.textInput.current.focus();
    this.props.setFocus(true);
  };

  handleOnIconClick = () => {
    if (this.props.searchQuery.length) {
      this.props.onChange('');
    }

    this.focusInput();
  };

  render() {
    return (
      <Container>
        <Grid>
          <Icon onClick={this.handleOnIconClick}>
            {this.props.searchQuery.length ? <IoMdClose /> : <IoMdSearch />}
          </Icon>
          <VerticalSeparator />
          <Input
            value={this.props.searchQuery}
            onChange={e => this.props.onChange(e.target.value)}
            onBlur={() => this.props.setFocus(false)}
            ref={this.textInput}
            placeholder="Type to search..."
          />
          {Boolean(this.props.tags.length) && (
            <TagList>
              <FiFilter />
              {this.props.tags.map(tag => (
                <Tag
                  onClick={() => this.props.removeTag(tag)}
                  title="Remove"
                  key={tag.text}
                >
                  {tag.text}
                </Tag>
              ))}
            </TagList>
          )}
        </Grid>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  searchQuery: state.homePageLinkFilters.searchQuery || '',
  focused: state.homePageLinkFilters.searchFieldFocused,
  tags: state.homePageLinkFilters.includedTags || [],
});
const mapDispatchToProps = dispatch => ({
  onChange: value => dispatch(updateSearchQuery(value)),
  setFocus: flag => dispatch(setFocusOnSearchField(flag)),
  removeTag: tag => dispatch(removeTagToFilter(tag)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Search);
