import React, { Component } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from '@emotion/styled/macro';
import { Link as RouterLink } from 'react-router-dom';
import { isInputFieldInFocus, destroyLocalCache } from '../../helpers/index';
import Search from './Search';
import { downloadJSON } from '../../Services/Downloader';
import { fetchMetaTags } from '../../Services/UrlMetadata';
import { TextButton } from '../../Components/Buttons';
import ListOfLinks from './ListOfLinks';
import { setLinkMeta } from '../../redux/links/index';

const QUICK_ITEMS_KEYS = [
  { keyCode: 49, itemPosition: 1 }, // Number 1 on keyboard
  { keyCode: 50, itemPosition: 2 }, // Number 2 on keyboard
  { keyCode: 51, itemPosition: 3 }, // Number 3 on keyboard
  { keyCode: 52, itemPosition: 4 }, // Number 4 on keyboard
  { keyCode: 53, itemPosition: 5 }, // Number 5 on keyboard
  { keyCode: 54, itemPosition: 6 }, // Number 6 on keyboard
  { keyCode: 55, itemPosition: 7 }, // Number 7 on keyboard
  { keyCode: 56, itemPosition: 8 }, // Number 8 on keyboard
  { keyCode: 57, itemPosition: 9 }, // Number 9 on keyboard
  { keyCode: 48, itemPosition: 10 }, // Number 0 on keyboard
];

const Toolbar = styled.section`
  height: 100px;
  max-width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin: 0 60px;
  a {
    display: flex;
    align-items: center;
    font-size: ${props => props.theme.fontSize.normal};
    color: ${props => props.theme.colors._6.hex};
    padding: 0 10px;
  }
`;
const Button = styled(TextButton)`
  padding: 0 10px;
  font-size: ${props => props.theme.fontSize.normal};
  color: ${props => props.theme.colors._6.hex};
`;

const Layout = styled.section`
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-left: 60px;
  padding-right: 60px;
  transition: all ${props => props.theme.animationDuration.normal};
  padding-bottom: 50px;
`;
const SearchStripe = styled.section`
  width: 100%;
  display: grid;
  grid-template-columns: 1fr;
`;
const Divider = styled.p`
  margin: 20px 0;
  padding: 0;
`;

class Home extends Component {
  static propTypes = {
    allLinks: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        url: PropTypes.string,
        id: PropTypes.string,
      }),
    ).isRequired,
    previewModeEnabled: PropTypes.bool.isRequired,
    controlModeEnabled: PropTypes.bool.isRequired,
    backup: PropTypes.instanceOf(Object).isRequired,
    factoryReset: PropTypes.func.isRequired,
    refreshSiteIcons: PropTypes.func.isRequired,
    downloadBackup: PropTypes.func.isRequired,
  };

  componentDidMount() {
    window.addEventListener('keyup', this.openLinkOnNumber);
  }

  componentWillUnmount() {
    window.removeEventListener('keyup', this.openLinkOnNumber);
  }

  openLinkOnNumber = e => {
    if (isInputFieldInFocus()) {
      return;
    }

    const quickItem = QUICK_ITEMS_KEYS.filter(
      ({ keyCode }) => keyCode === e.keyCode,
    ).reduce((prev, next) => next, null);

    if (!quickItem) {
      return;
    }

    const index = quickItem.itemPosition - 1;
    const link = this.props.links[index];

    if (!link) {
      return;
    }

    window.location.href = link.url;
  };

  handleOnLinkClick = (e, linkId) => {
    if (this.props.controlModeEnabled) {
      e.preventDefault();
      e.stopPropagation();
      return;
    }

    if (linkId) {
      this.props.recordLinkClick(linkId);
    }
  };

  render() {
    return (
      <section>
        {this.props.controlModeEnabled && (
          <div>
            <Toolbar>
              <Button
                title="Create Backup"
                onClick={() => this.props.downloadBackup(this.props.backup)}
              >
                Create Backup
              </Button>
              <RouterLink
                title="Add New Link"
                to="/backup/restore"
                href="/backup/restore"
              >
                Restore Backup
              </RouterLink>
              <Button
                title="Refresh Icons for Custom Links"
                onClick={() => this.props.refreshSiteIcons(this.props.allLinks)}
              >
                Refresh Site Icons
              </Button>
              <Button
                title="Refresh Icons for Custom Links"
                onClick={this.props.factoryReset}
              >
                Reset to factory defaults
              </Button>
              <RouterLink
                title="Settings page"
                to="/preferences"
                href="/preferences"
              >
                Preferences
              </RouterLink>
            </Toolbar>
          </div>
        )}
        <Layout>
          <SearchStripe>
            <Search />
          </SearchStripe>
          <Divider />
          <ListOfLinks />
        </Layout>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  searchInput: state.searchInput,
  previewModeEnabled: state.viewMode.previewMode,
  controlModeEnabled: state.viewMode.controlMode,
  openLinksInNewTab: state.preferences.openLinksInNewTab,
  backup: state,
  allLinks: state.links.custom,
});
const mapDispatchToProps = dispatch => ({
  downloadBackup: state => {
    const filename = `${moment().format()}.△`;
    const __meta = { createdAt: moment().format() };
    downloadJSON({ ...state, __meta }, filename);
  },
  factoryReset: () => {
    if (!window.confirm('Are you sure')) {
      return;
    }
    destroyLocalCache();
    window.location.href = '/';
  },
  refreshSiteIcons: links => {
    links.forEach(async ({ url, id }) => {
      const metaData = await fetchMetaTags(url, true);

      dispatch(setLinkMeta(id, metaData));
    });
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
