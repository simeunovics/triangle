import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from '@emotion/styled/macro';
import { IoIosRemoveCircleOutline } from 'react-icons/io';
import { FiArrowUpRight } from 'react-icons/fi';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { addTagToFilter } from '../../redux/homePageLinkFilters/index';
import { RichLink } from '../../Components/RichLink';
import {
  Favicon,
  LinkAvatar,
  Content,
  Title,
  Description,
  Links,
  InlineEdit,
} from '../../Components/RichLink';
import {
  removeLink,
  editLinkName,
  editLinkUrl,
  recordLinkClick,
} from '../../redux/links/index';
import { fuzzySearch } from '../../helpers';
import emptyPageIllustration from './empty.svg';

const FAVICON_IMAGE_SIZE = 30;

const RemoveLinkButton = styled.button`
  background-color: transparent;
  border: none;
  font-size: ${FAVICON_IMAGE_SIZE}px;
  font-weight: bold;
  cursor: no-drop;
  color: ${props => props.theme.colors._8.hex};
  padding: 0;
  margin: 0;
  width: ${FAVICON_IMAGE_SIZE}px;
  height: ${FAVICON_IMAGE_SIZE}px;
`;
const ExternalLinkIcon = styled.div`
  transition: all ${props => props.theme.animationDuration.normal};
  color: ${props => props.theme.colors._6.hex};
  opacity: 0;
  position: absolute;
  top: 2px;
  right: 2px;
`;
const RelativePositionedLink = styled(RichLink)`
  position: relative;
  :hover {
    ${ExternalLinkIcon} {
      opacity: ${props => props.theme.opacity.low};
    }
  }
`;
const Link = styled.div``;
const Tags = styled.section`
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: flex-end;
  a {
    font-size: ${props => props.theme.fontSize.small};
    color: ${props => props.theme.colors._4.hex};
    padding-left: 10px;
  }
`;
const Tag = styled.div`
  transition: opacity ${props => props.theme.animationDuration.short};
  opacity: ${props => props.theme.opacity.low};
  :hover {
    opacity: ${props => props.theme.opacity.high};
  }
`;
const TagColorIndicator = styled.div`
  margin: 2px;
  width: 14px;
  height: 7px;
  background-color: ${props => props.tagColor};
  border-radius: 1px;
`;

const EmptyPageIllustrationHolder = styled.div`
  text-align: center;
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  h2 {
    color: ${props => props.theme.colors._4.hex};
  }
  img {
    user-select: none;
    width: 40%;
  }
`;
const EmptyListOfLinks = () => (
  <EmptyPageIllustrationHolder>
    <img src={emptyPageIllustration} alt="Empty page illustration" />
    <h2>To add new link simply paste it here</h2>
  </EmptyPageIllustrationHolder>
);

class ListOfLinks extends Component {
  static propTypes = {
    links: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        url: PropTypes.string,
        id: PropTypes.string,
      }),
    ).isRequired,
    removeLink: PropTypes.func.isRequired,
    editLinkName: PropTypes.func.isRequired,
    editLinkUrl: PropTypes.func.isRequired,
    recordLinkClick: PropTypes.func.isRequired,
    previewModeEnabled: PropTypes.bool.isRequired,
    controlModeEnabled: PropTypes.bool.isRequired,
    openLinksInNewTab: PropTypes.bool.isRequired,
    showEmptyPageIllustration: PropTypes.bool.isRequired,
  };
  handleOnLinkClick = (e, linkId) => {
    if (this.props.controlModeEnabled) {
      e.preventDefault();
      e.stopPropagation();
      return;
    }

    if (linkId) {
      this.props.recordLinkClick(linkId);
    }
  };
  render() {
    if (this.props.showEmptyPageIllustration) {
      return <EmptyListOfLinks />;
    }
    return (
      <Links>
        {this.props.links.map((link, index) => {
          return (
            <Link key={link.id}>
              <RelativePositionedLink
                onClick={e => this.handleOnLinkClick(e, link.id)}
                title={link.url}
                href={link.url}
                target={this.props.openLinksInNewTab ? '_blank' : ''}
              >
                <Favicon>
                  {this.props.previewModeEnabled ? (
                    <RemoveLinkButton
                      title="Remove This Link"
                      onClick={e => {
                        e.preventDefault();
                        this.props.removeLink(link.id);
                      }}
                    >
                      <IoIosRemoveCircleOutline />
                    </RemoveLinkButton>
                  ) : (
                      <LinkAvatar name={link.name} icon={link.metadata.icon} />
                    )}
                </Favicon>
                {this.props.controlModeEnabled ? (
                  <Content>
                    <Title>
                      <InlineEdit
                        value={link.name}
                        onChange={e =>
                          this.props.editLinkName(link.id, e.target.value)
                        }
                      />
                    </Title>
                    <Description>
                      <InlineEdit
                        value={link.url}
                        onChange={e =>
                          this.props.editLinkUrl(link.id, e.target.value)
                        }
                      />
                    </Description>
                  </Content>
                ) : (
                    <Content>
                      <Title>{link.name}</Title>
                      <Description>{link.url}</Description>
                    </Content>
                  )}
                {this.props.openLinksInNewTab && (
                  <ExternalLinkIcon>
                    <FiArrowUpRight />
                  </ExternalLinkIcon>
                )}
              </RelativePositionedLink>
              <Tags>
                {link.tags.map(tag => (
                  <Tag
                    key={tag.id}
                    onClick={() =>
                      this.props.addTagToFilter({
                        text: tag.text,
                        color: tag.color,
                      })
                    }
                  >
                    <TagColorIndicator title={tag.text} tagColor={tag.color} />
                  </Tag>
                ))}
                {this.props.controlModeEnabled && (
                  <RouterLink
                    title="Manage Tags"
                    to={`/links/${link.id}/manage-tags`}
                    href={`/links/${link.id}/manage-tags`}
                  >
                    <span>manage tags</span>
                  </RouterLink>
                )}
              </Tags>
            </Link>
          );
        })}
      </Links>
    );
  }
}
const mapStateToProps = state => ({
  previewModeEnabled: state.viewMode.previewMode,
  controlModeEnabled: state.viewMode.controlMode,
  openLinksInNewTab: state.preferences.openLinksInNewTab,
  showEmptyPageIllustration: state.links.custom.length === 0,
  links: (() => {
    const { searchQuery, includedTags = [] } = state.homePageLinkFilters;
    let filteredLinks = state.links.custom.filter(reference =>
      fuzzySearch(searchQuery, reference),
    );

    if (Boolean(includedTags.length)) {
      const includeThisTagNames = includedTags.map(({ text }) => text);

      filteredLinks = filteredLinks.filter(({ tags = [] }) => {
        return Boolean(
          tags.filter(({ text }) => includeThisTagNames.includes(text)).length,
        );
      });
    }

    if (state.preferences.sortLinksByUsage) {
      filteredLinks.sort((a, b) => {
        const usageForLinkA = a.clicks;
        const usageForLinkB = b.clicks;

        return usageForLinkB.length - usageForLinkA.length;
      });

      return filteredLinks;
    }

    return filteredLinks;
  })(),
});
const mapDispatchToProps = dispatch => ({
  removeLink: url => dispatch(removeLink(url)),
  editLinkName: (id, name) => dispatch(editLinkName(id, name)),
  editLinkUrl: (id, url) => dispatch(editLinkUrl(id, url)),
  recordLinkClick: linkId => dispatch(recordLinkClick(linkId)),
  addTagToFilter: tag => dispatch(addTagToFilter(tag)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListOfLinks);
