import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from '@emotion/styled';
import uuidv4 from 'uuid/v4';
import { addTag, removeTag } from '../../redux/links/index';
import Modal from '../../Components/Modal';
import { Input, Row, Col, Typography, Button, Tag } from 'antd';

const Page = styled.section`
  padding: 50px;
`;

class TagManager extends Component {
  state = {
    text: '',
    color: '#c2c2c2',
  };
  updateText = e => this.setState({ text: e.target.value });
  updateColor = e => this.setState({ color: e.target.value });
  addNewTag = () => {
    const tagText = this.state.text;
    if (!Boolean(tagText.length)) {
      window.alert('Tag text can not be empty!');
      return;
    }
    this.props.addTag(this.props.match.params.linkId, {
      id: uuidv4(),
      text: tagText,
      color: this.state.color,
    });
    this.setState({ text: '' });
  };
  suggestedTags = (existingTags = []) => {
    const tags = new Map();
    this.props.links
      .map(link => link.tags)
      .reduce((prev = [], next) => [...prev, ...next])
      .forEach(tag => tags.set(tag.text, tag));
    existingTags.forEach(tag => tags.delete(tag.text));

    const ret = [];
    for (let tag of tags.values()) {
      ret.push(tag);
    }
    return ret;
  };
  render() {
    const link = this.props.links
      .filter(link => link.id === this.props.match.params.linkId)
      .reduce((prev, next) => ({ ...prev, ...next }), {});

    return (
      <Modal>
        <Page>
          <div>
            <Typography.Title>Add new tag</Typography.Title>
            <Row>
              <Col span={6}>
                <Input.Group compact>
                  <Input
                    style={{ width: '90%' }}
                    type="text"
                    value={this.state.text}
                    onChange={this.updateText}
                  />
                  <Input
                    style={{ width: '10%' }}
                    type="color"
                    value={this.state.color}
                    onChange={this.updateColor}
                  />
                </Input.Group>
              </Col>
              <Col span={3}>
                <Button onClick={this.addNewTag}>Add</Button>
              </Col>
            </Row>
          </div>
          <div>
            <Typography.Title level={2}>Suggested tags</Typography.Title>
            {this.suggestedTags(link.tags).map(tag => (
              <Tag
                key={tag.id}
                color={tag.color}
                onClick={() =>
                  this.props.addTag(link.id, {
                    id: uuidv4(),
                    text: tag.text,
                    color: tag.color,
                  })
                }
              >
                {tag.text}
              </Tag>
            ))}
          </div>
          <div>
            <Typography.Title level={2}>List of existing tags</Typography.Title>
            <Row>
              <Col>
                {link.tags.map(tag => {
                  return (
                    <Tag
                      key={tag.id}
                      color={tag.color}
                      onClick={() => this.props.removeTag(link.id, tag.id)}
                    >
                      {tag.text}
                    </Tag>
                  );
                })}
              </Col>
            </Row>
          </div>
          <hr />
          <Row>
            <Col>
              <Button size="large" onClick={() => this.props.history.goBack()}>
                Back
              </Button>
            </Col>
          </Row>
        </Page>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  links: state.links.custom,
});
const mapDispatchToProps = dispatch => ({
  addTag: (linkId, tag) => dispatch(addTag(linkId, tag)),
  removeTag: (linkId, tagId) => dispatch(removeTag(linkId, tagId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TagManager);
