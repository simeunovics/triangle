import React, { useState } from 'react';
import moment from 'moment';
import styled from '@emotion/styled';
import { persistState, applyTransitions } from '../../helpers/index';
import { Upload, Icon, message, List, Avatar, Button } from 'antd';
import TopNavigation from '../../Components/TopNavigation';

const Page = styled.section`
  min-height: calc(100vh - 85px);
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 50%;
  margin: 0 auto;
`;

const RestoreBackup: React.FC = () => {
  const [uploadedFile, setUploadedFile] = useState<any | null>(null);

  if (uploadedFile) {
    return (
      <>
        <TopNavigation />
        <Page>
          <List
            bordered={true}
            size="large"
            itemLayout="horizontal"
            dataSource={[uploadedFile]}
            renderItem={item => (
              <List.Item
                actions={[
                  <Button onClick={() => setUploadedFile(null)}>Cancel</Button>,
                  <Button
                    type="danger"
                    onClick={() => {
                      persistState(applyTransitions(item));
                      window.alert('Backup successfully restored!');
                      window.location.href = '/';
                    }}
                  >
                    Restore
                  </Button>,
                ]}
              >
                <List.Item.Meta
                  avatar={<Avatar icon="history" />}
                  title={
                    'Created ' +
                    moment(
                      item.__meta ? item.__meta.createdAt : undefined,
                    ).fromNow()
                  }
                />
              </List.Item>
            )}
          />
        </Page>
      </>
    );
  }

  return (
    <>
      <TopNavigation />
      <Page>
        <Upload.Dragger
          showUploadList={false}
          listType="picture-card"
          beforeUpload={file => {
            const reader = new FileReader();
            reader.onload = () => {
              try {
                const stringifiedBackup = String(reader.result);
                if (!Boolean(stringifiedBackup.trim().length)) {
                  message.error(
                    'Unable to parse backup! Looks like backup file is empty!',
                  );
                  return false;
                }

                const backupJson = JSON.parse(stringifiedBackup);
                if (!Boolean(backupJson)) {
                  message.error(
                    'Unable to parse backup! Looks like it is not a valid JSON backup!',
                  );
                  return false;
                }

                setUploadedFile(backupJson);
              } catch ({ message: error }) {
                message.error(error);
              }
              return false;
            };
            reader.onabort = () => message.error('File reading was aborted');
            reader.onerror = () => message.error('File reading has failed');
            reader.readAsText(file);

            return false;
          }}
        >
          <div>
            <Icon type="plus" />
            <div className="ant-upload-text">Upload</div>
          </div>
        </Upload.Dragger>
      </Page>
    </>
  );
};

export default RestoreBackup;
