import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styled from '@emotion/styled/macro';
import { TiLockClosedOutline, TiLockOpenOutline } from 'react-icons/ti';
import { toggleControlMode } from '../redux/viewMode/index';
import { TextButton } from '../Components/Buttons';

const Button = styled(TextButton)`
  transition: all ${props => props.theme.animationDuration.short};
  font-size: ${props => props.theme.fontSize.large};
  position: fixed;
  bottom: 25px;
  left: 25px;
  color: ${props => props.theme.colors._6.hex};
  opacity: ${props => props.theme.opacity.low};
  :hover {
    opacity: ${props => props.theme.opacity.medium};
  }
`;

const ControlModeIndicator = props => (
  <Button title="Toggle control mode" onClick={props.onClick}>
    {props.enabled ? <TiLockOpenOutline /> : <TiLockClosedOutline />}
  </Button>
);
ControlModeIndicator.propTypes = {
  enabled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  enabled: state.viewMode.controlMode,
});
const mapDispatchToProps = dispatch => ({
  onClick: () => dispatch(toggleControlMode()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ControlModeIndicator);
