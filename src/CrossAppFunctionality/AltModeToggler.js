import { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  toggleControlMode,
  enablePreviewMode,
  disablePreviewMode,
} from '../redux/viewMode/index';

const ALT_MODE_ACTIVATION_KEY_CODES = [18]; // Alt
const ALT_MODE_PREVIEW_KEY_CODES = [18]; // Alt
const ALT_MODE_ENTRANCE_DELAY = 200; // 0.2 sec
const ALT_MODE_NUMBER_OF_KEYSTROKES = 2;
const ALT_MODE_KEY_HOLD_DELAY_TIMEOUT = 90; // 0.09 sec

class AltModeToggler extends Component {
  static propTypes = {
    toggleControlMode: PropTypes.func.isRequired,
    enablePreviewMode: PropTypes.func.isRequired,
    disablePreviewMode: PropTypes.func.isRequired,
  };

  constructor() {
    super();
    this.numberOfTimesAltKeyWasPressed = 0;
    this.altModeKeyHoldDelayTimeout = null;
  }

  componentDidMount() {
    window.addEventListener('keyup', this.toggleControlMode);
    window.addEventListener('keydown', this.enableAltPreview);
    window.addEventListener('keyup', this.disableAltPreview);
  }

  componentWillUnmount() {
    window.removeEventListener('keyup', this.toggleControlMode);
    window.removeEventListener('keydown', this.enableAltPreview);
    window.removeEventListener('keyup', this.disableAltPreview);
  }

  toggleControlMode = e => {
    if (!ALT_MODE_ACTIVATION_KEY_CODES.includes(e.keyCode)) {
      return;
    }
    this.numberOfTimesAltKeyWasPressed += 1;

    if (this.numberOfTimesAltKeyWasPressed >= ALT_MODE_NUMBER_OF_KEYSTROKES) {
      this.props.toggleControlMode();
      this.numberOfTimesAltKeyWasPressed = 0;

      return;
    }

    window.setTimeout(() => {
      if (this.numberOfTimesAltKeyWasPressed) {
        this.numberOfTimesAltKeyWasPressed -= 1;
      }
    }, ALT_MODE_ENTRANCE_DELAY);
  };

  enableAltPreview = e => {
    if (!ALT_MODE_PREVIEW_KEY_CODES.includes(e.keyCode)) {
      return;
    }
    if (this.altModeKeyHoldDelayTimeout) {
      window.clearTimeout(this.altModeKeyHoldDelayTimeout);
    }

    this.altModeKeyHoldDelayTimeout = window.setTimeout(
      this.props.enablePreviewMode,
      ALT_MODE_KEY_HOLD_DELAY_TIMEOUT,
    );
  };

  disableAltPreview = e => {
    if (!ALT_MODE_PREVIEW_KEY_CODES.includes(e.keyCode)) {
      return;
    }

    if (this.altModeKeyHoldDelayTimeout) {
      window.clearTimeout(this.altModeKeyHoldDelayTimeout);
    }

    this.props.disablePreviewMode();
  };

  render() {
    return null;
  }
}

const mapDispatchToProps = dispatch => ({
  toggleControlMode: () => dispatch(toggleControlMode()),
  enablePreviewMode: () => dispatch(enablePreviewMode()),
  disablePreviewMode: () => dispatch(disablePreviewMode()),
});

export default connect(
  null,
  mapDispatchToProps,
)(AltModeToggler);
