import axios from 'axios';

const PAGE_ANALYZER_URL = 'https://page-analytics.herokuapp.com/';
const DEFAULT_METADATA_OBJECT = {
  icon: null,
  provider: null,
};
class UrlMetadata {
  fetchMetadata = async url => {
    try {
      const { data } = await axios.get(`${PAGE_ANALYZER_URL}?url=${url}`);
      const { favicons, title: provider } = data;

      const { icons } = favicons;
      const icon = icons[0].url || null;

      const meta = {
        icon,
        provider,
      };

      return meta;
    } catch (e) {
      window.console.error(e);

      return DEFAULT_METADATA_OBJECT;
    }
  };
}

const metadataInstance = new UrlMetadata();

export const fetchMetaTags = (url, force) =>
  metadataInstance.fetchMetadata(url, force);
