import React from 'react';
import { Link } from 'react-router-dom';
import styled from '@emotion/styled/macro';
import { MdKeyboardBackspace } from 'react-icons/md';
import { ButtonGroup } from './Buttons';

const Navigation = styled.nav`
  max-width: 100%;
  height: 50px;
  padding-left: 20px;
  display: flex;
  align-items: center;
  a {
    color: ${props => props.theme.colors._5.hex};
    display: flex;
    align-items: center;
  }
`;

const TopNavigation = () => (
  <Navigation>
    <ButtonGroup>
      <Link alt="Home" title="Home" to="/" href="/">
        <MdKeyboardBackspace />
        <span style={{ marginLeft: 5 }}>Back</span>
      </Link>
    </ButtonGroup>
  </Navigation>
);

export default TopNavigation;
