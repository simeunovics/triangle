import React, { Component } from 'react';
import styled from '@emotion/styled/macro';
import PropTypes from 'prop-types';
import { fetchMetaTags } from '../Services/UrlMetadata';
import {
  RichLink,
  Favicon,
  LinkAvatar,
  Content,
  Title as ShortTitle,
  Description as ShortDesc,
  InlineEdit,
} from './RichLink';
import { RoundedButton, ButtonGroup } from './Buttons';
import { isInputFieldInFocus } from '../helpers/index';

const CONTAINER_HEIGHT = 75;
const BUTTON_GROUP_WIDTH = 180;
const LINK_WIDTH = 500;
const PADDING = 20;

const Stripe = styled.div`
  width: 100%;
  height: ${CONTAINER_HEIGHT}px;
  position: fixed;
  bottom: ${PADDING}px;
  right: ${PADDING}px;
  display: flex;
  justify-content: flex-end;
  z-index: 1;
`;

const Body = styled.div`
  display: grid;
  grid-template-columns: ${LINK_WIDTH}px ${BUTTON_GROUP_WIDTH}px ${PADDING}px;
`;
const Title = styled(ShortTitle)`
  width: 90%;
`;
const Description = styled(ShortDesc)`
  width: 90%;
`;

class QuickLinkForm extends Component {
  static propTypes = {
    addLink: PropTypes.func.isRequired,
    cacheMeta: PropTypes.func.isRequired,
  };

  state = {
    name: null,
    url: null,
    icons: {},
  };

  componentDidMount() {
    window.document
      .getElementsByTagName('html')[0]
      .addEventListener('paste', this.onPasteHandler);
  }

  componentWillUnmount() {
    window.document
      .getElementsByTagName('html')[0]
      .removeEventListener('paste', this.onPasteHandler);
  }

  onPasteHandler = e => {
    if (isInputFieldInFocus()) {
      return;
    }
    const url = (e.originalEvent || e).clipboardData.getData('text/plain');

    try {
      const link = new URL(url);

      this.reset();
      this.setState({ url, name: link.hostname });

      this.fetchIcon(url);
    } catch (exception) {
      // pass
    }
  };

  fetchIcon = url => {
    fetchMetaTags(url).then(meta => {
      this.setState(currentState => ({
        icons: {
          ...currentState.icons,
          [url]: meta.icon,
        },
      }));
    });
  };

  reset = () => {
    this.setState({ url: null, name: null });
  };

  addLink = async () => {
    const { url, name } = this.state;
    this.reset();

    this.props.addLink(name, url);
    this.props.cacheMeta(url, await fetchMetaTags(url));
  };

  resolveIcon = url => this.state.icons[url] || null;

  handleNameUpdate = name => {
    this.setState({ name });
  };

  handleUrlUpdate = url => {
    this.setState({ url });
    this.fetchIcon(url);
  };

  render() {
    if (!this.state.url && !this.state.name) {
      return null;
    }

    return (
      <Stripe>
        <RichLink onClick={() => false}>
          <Favicon>
            <LinkAvatar
              name={this.state.url}
              icon={this.resolveIcon(this.state.url)}
            />
          </Favicon>
          <Body>
            <Content>
              <Title>
                <InlineEdit
                  value={this.state.name}
                  onChange={e => this.handleNameUpdate(e.target.value)}
                />
              </Title>
              <Description>
                <InlineEdit
                  value={this.state.url}
                  onChange={e => this.handleUrlUpdate(e.target.value)}
                />
              </Description>
            </Content>
            <ButtonGroup>
              <RoundedButton onClick={this.addLink}>Add</RoundedButton>
              <RoundedButton
                style={{ marginLeft: PADDING }}
                onClick={this.reset}
              >
                Cancel
              </RoundedButton>
            </ButtonGroup>
          </Body>
        </RichLink>
      </Stripe>
    );
  }
}

export default QuickLinkForm;
