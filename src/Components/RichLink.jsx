import React from 'react';
import styled from '@emotion/styled/macro';
import PropTypes from 'prop-types';
import { TiImage } from 'react-icons/ti';
import Img from '../Components/Img';

const FAVICON_BOX_SIZE = 75;
const FAVICON_IMAGE_SIZE = 30;
const BOX_MIN_WIDTH = FAVICON_BOX_SIZE * 4;
const SPACE_BETWEEN_BOXES = 40;

export const Links = styled.section`
  display: grid;
  grid-gap: ${SPACE_BETWEEN_BOXES}px;
  grid-template-columns: repeat(auto-fill, minmax(${BOX_MIN_WIDTH}px, 1fr));
`;
export const RichLink = styled.a`
  display: grid;
  background-color: ${props => props.theme.colors._3.hex};
  border-radius: 10px;
  overflow: hidden;
  grid-column-gap: 15px;
  grid-template-columns: ${FAVICON_BOX_SIZE}px 1fr;
`;
export const InlineEdit = styled.input`
  background-color: transparent;
  border: none;
  outline: none;
  width: 90%;
`;
export const Title = styled.p`
  color: ${props => props.theme.colors._7.hex};
  padding: 0;
  margin: 0;
  font-size: ${props => props.theme.fontSize.normal};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: calc(${BOX_MIN_WIDTH}px - ${FAVICON_BOX_SIZE}px - 20px);
  ${InlineEdit} {
    font-size: ${props => props.theme.fontSize.normal};
    color: ${props => props.theme.colors._7.hex};
  }
`;
export const Description = styled.p`
  padding: 0;
  margin: 0;
  font-size: small;
  color: ${props => props.theme.colors._4.hex};
  font-size: ${props => props.theme.fontSize.small};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: calc(${BOX_MIN_WIDTH}px - ${FAVICON_BOX_SIZE}px - 20px);
  ${InlineEdit} {
    font-size: ${props => props.theme.fontSize.small};
    color: ${props => props.theme.colors._4.hex};
  }
`;
export const LinkAvatar = ({ name, icon }) => {
  if (icon) {
    return (
      <Img
        alt={name}
        src={icon}
        fallback={() => <TiImage style={{ opacity: 0.4 }} />}
      />
    );
  }

  return <TiImage style={{ opacity: 0.4 }} />;
};
LinkAvatar.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.string,
};
LinkAvatar.defaultProps = {
  name: '',
  icon: null,
};

export const Favicon = styled.figure`
  width: ${FAVICON_BOX_SIZE}px;
  height: ${FAVICON_BOX_SIZE}px;
  margin: 0;
  padding: 0px;
  color: ${props => props.theme.colors._4.hex};
  background-color: ${props => props.theme.colors._2.hex};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${props => props.theme.fontSize.large};
  img {
    width: ${FAVICON_IMAGE_SIZE}px;
    height: ${FAVICON_IMAGE_SIZE}px;
  }
`;
export const Content = styled.section`
  flex: 1;
  padding: 0;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
`;
