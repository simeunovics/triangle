import * as React from 'react';

interface ImgProps {
  src: string;
  alt: string;
  fallback: Function;
}

class Img extends React.Component<ImgProps> {
  state = {
    fallback: false,
  };
  onError = () => {
    this.setState({ fallback: true });
  };
  render() {
    const { src, alt, fallback, ...rest } = this.props;
    return this.state.fallback ? (
      fallback()
    ) : (
      <img alt={alt} src={src} {...rest} onError={this.onError} />
    );
  }
}

export default Img;
