import React from 'react';
import styled from '@emotion/styled';

const Backdrop = styled.div`
  width: 100%;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.8);
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9;
`;

const Modal = ({ children }) => <Backdrop> {children} </Backdrop>;

export default Modal;
