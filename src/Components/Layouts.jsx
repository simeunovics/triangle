import styled from '@emotion/styled/macro';

export const PageWithTopNav = styled.div`
  display: grid;
  width: 100%;
  grid-template-columns: 1fr;
  grid-template-rows: 50px 1fr;
`;
