import styled from '@emotion/styled/macro';

const Button = styled.button`
  border: none;
  padding: none;
  padding: 0;
  margin: 0;
  cursor: pointer;
  outline: none;
  background-color: transparent;
`;
export const RoundedButton = styled(Button)`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px 20px;
  border-radius: 10px;
  background-color: ${props => props.theme.colors._2.hex};
  color: ${props => props.theme.colors._4.hex};
  font-size: ${props => props.theme.fontSize.normal};
`;
export const ButtonGroup = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: flex-start;
`;
export const TextButton = styled(Button)`
  display: flex;
  align-items: center;
  justify-content: center;
`;
