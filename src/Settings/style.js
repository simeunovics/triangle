import convert from 'color-convert';

const lightMode = [
  { name: '_1', value: '#ffffff' },
  { name: '_2', value: '#f0f3f4' },
  { name: '_3', value: '#f8fafa' },
  { name: '_4', value: '#919499' },
  { name: '_5', value: '#5e6266' },
  { name: '_6', value: '#919499' },
  { name: '_7', value: '#5e6266' },
  { name: '_8', value: '#FF0000' },
];

const darkMode = [
  { name: '_1', value: '#2a2a2e' },
  { name: '_2', value: '#28282b' },
  { name: '_3', value: '#28282b' },
  { name: '_4', value: '#919499' },
  { name: '_5', value: '#5e6266' },
  { name: '_6', value: '#919499' },
  { name: '_7', value: '#919499' },
  { name: '_8', value: '#FF0000' },
];

const createColorPallette = theme => {
  return theme
    .map(({ name: colorName, value: hexValue }) => ({
      [colorName]: {
        hex: hexValue,
        rgb: () => {
          const [r, g, b] = convert.hex.rgb(hexValue);
          return `rgb(${r}, ${g}, ${b})`;
        },
        rgba: opacity => {
          const [r, g, b] = convert.hex.rgb(hexValue);
          return `rgba(${r}, ${g}, ${b}, ${opacity})`;
        },
        hsla: opacity => {
          const [h, s, l] = convert.hex.hsl(hexValue);
          return `hsla(${h}, ${s}%, ${l}%, ${opacity})`;
        },
      },
    }))
    .reduce((prev, next) => ({ ...prev, ...next }), {});
};

const defaults = {
  fontSize: {
    tiny: '10px',
    small: '13px',
    normal: '17px',
    large: '24px',
    huge: '36px',
  },
  opacity: {
    low: 0.2,
    medium: 0.5,
    high: 0.7,
  },
  animationDuration: {
    short: '0.2s',
    normal: '0.4s',
    long: '0.2s',
  },
  shadowOpacity: {
    low: 0.2,
    normal: 0.5,
    high: 0.7,
  },
};
export const theme = {
  Light: {
    ...defaults,
    colors: createColorPallette(lightMode),
  },
  Dark: {
    ...defaults,
    colors: createColorPallette(darkMode),
  },
};
