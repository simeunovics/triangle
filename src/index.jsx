import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter as Router, Route, Redirect } from 'react-router-dom';
import { ThemeProvider } from 'emotion-theming';
import { Global, css } from '@emotion/core';
import './index.css';
import store from './redux/index';
import HomePage from './Pages/Home/index';
import AltModeToggler from './CrossAppFunctionality/AltModeToggler';
import RestoreBackupPage from './Pages/RestoreBackup/index';
import QuickLink from './CrossAppFunctionality/QuickLink';
import ControlModeIndicator from './CrossAppFunctionality/ControlModeIndicator';
import PreferencesPage from './Pages/Preferences';
import TagManager from './Pages/TagManager/index';
import { theme } from './Settings/style';
import Body from './Components/Body';
const userSelectedColorTheme = theme[store.getState().preferences.theme];

ReactDOM.render(
  <React.StrictMode>
    <Global
      styles={css`
        :root {
          --main-bg-color: ${userSelectedColorTheme.colors._1.hex};
        }
      `}
    />
    <Provider store={store}>
      <Router>
        <ThemeProvider theme={userSelectedColorTheme}>
          <Body>
            <ControlModeIndicator />
            <AltModeToggler />
            <QuickLink />
            <Route exact path="/" render={() => <Redirect to="/links" />} />
            <Route path="/links" component={HomePage} />
            <Route path="/links/:linkId/manage-tags" component={TagManager} />
            <Route exact path="/preferences" component={PreferencesPage} />
            <Route exact path="/backup/restore" component={RestoreBackupPage} />
          </Body>
        </ThemeProvider>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
